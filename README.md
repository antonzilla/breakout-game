Simple game example that uses common game patterns:
* encapsulation of logic in a game object;
* game loop with fixed update rate and variable render rate;
* update method.

The HTML5 Canvas API is used to render the game.
