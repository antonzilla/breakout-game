import {Rect} from './physics';

export default class Brick {

  constructor(x, y, width, height) {
    this.destroyed = false;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  destroy() {
    this.destroyed = true;
  }

  draw(ctx) {
    if (!this.destroyed) {
      ctx.beginPath();
      ctx.rect(this.x, this.y, this.width, this.height);
      ctx.fillStyle = '#0095DD';
      ctx.fill();
      ctx.closePath();
    }
  }

  update() {
  }

  getShape() {
    return new Rect(this.x, this.y, this.width, this.height);
  }
}
