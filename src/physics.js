export class Rect {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  get top() {
    return this.y;
  }

  get bottom() {
    return this.y + this.height;
  }

  get left() {
    return this.x;
  }

  get right() {
    return this.x + this.width;
  }
}

export class Circle {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }
}

export class Line {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  computeValue(x, y) {
    return (x - this.x1) / (this.x2 - this.x1) - (y - this.y1) / (this.y2 - this.y1);
  }
}

export default function overlaps(shape1, shape2) {
  if (shape1 instanceof Circle && shape2 instanceof Rect) {
    const distToCenter = {
      x: Math.abs(shape1.x - (shape2.x + shape2.width / 2)),
      y: Math.abs(shape1.y - (shape2.y + shape2.height / 2))
    };

    if (distToCenter.x > shape2.width / 2 + shape1.r || distToCenter.y > shape2.height / 2 + shape1.r) {
      return false;
    }

    if (distToCenter.x <= shape2.width / 2 || distToCenter.y <= shape2.height / 2) {
      return true;
    }

    const distToCornerSq = Math.pow(distToCenter.x - shape2.width / 2, 2) + Math.pow(distToCenter.y - shape2.height / 2, 2);
    return distToCornerSq <= Math.pow(shape1.r, 2);
  }

  if (shape1 instanceof Circle && shape2 instanceof Line) {
    const dist = Math.abs((shape2.y2 - shape2.y1) * shape1.x - (shape2.x2 - shape2.x1) * shape1.y + shape2.x2 * shape2.y1 - shape2.y2 * shape2.x1) / Math.sqrt(Math.pow(shape2.y2 - shape2.y1, 2) + Math.pow(shape2.x2 - shape2.x1, 2));
    return dist <= shape1.r;
  }

  if (shape1 instanceof Rect && shape2 instanceof Line) {
    const corners = [
      shape2.computeValue(shape1.left, shape1.top),
      shape2.computeValue(shape1.right, shape1.top),
      shape2.computeValue(shape1.left, shape1.bottom),
      shape2.computeValue(shape1.right, shape1.bottom)
    ];
    return !corners.every(v => v > 0) && !corners.every(v => v < 0);
  }

  throw new Error(`Not support overlap detection for ${shape1.constructor.name} and ${shape2.constructor.name}`);
}
