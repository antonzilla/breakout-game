import GameWorld from './game-world';

const world = new GameWorld(document.getElementById('myCanvas'));
document.addEventListener('keydown', e => {
  if (e.code === 'KeyP') {
    if (world.gameState === 'started') {
      world.pause();

    } else if (world.gameState === 'paused') {
      world.resume();
    }

  } else if (e.code === 'KeyS') {
    world.restart();
  }
});
