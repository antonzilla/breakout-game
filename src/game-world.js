import Ball from './ball';
import Paddle from './paddle';
import Brick from './brick';

export default class GameWorld {

  static BRICK_WIDTH = 75;
  static BRICK_HEIGHT = 20;
  static BRICK_PADDING = 10;
  static BRICK_OFFSET_TOP = 30;
  static BRICK_OFFSET_LEFT = 30;
  static BRICK_ROW_COUNT = 3;
  static BRICK_COLUMN_COUNT = 5;

  static MS_PER_UPDATE = 14;

  constructor(canvas) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');

    this.gameState = 'stopped';
    this.generate();
    this.render(0);

    const keyDownHandler = (e) => {
      if (this.gameState !== 'started') {
        return;
      }
      if (e.key === 'Right' || e.key === 'ArrowRight') {
        this.rightPressed = true;
      } else if (e.key === 'Left' || e.key === 'ArrowLeft') {
        this.leftPressed = true;
      }
    };

    const mouseMoveHandler = (e) => {
      if (this.gameState !== 'started') {
        return;
      }
      const relativeX = e.clientX - canvas.offsetLeft;
      if (relativeX > 0 && relativeX < canvas.width) {
        this.paddleRelativeX = relativeX;
      }
    };

    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('mousemove', mouseMoveHandler, false);
  }

  /**
   * @private
   */
  generate() {
    this.leftPressed = false;
    this.rightPressed = false;
    this.paddleRelativeX = undefined;
    this.score = 0;
    this.lives = 3;
    this.objects = [];
    this.objects.push(
      new Paddle(this),
      new Ball(this)
    );
    for (let c = 0; c < GameWorld.BRICK_COLUMN_COUNT; ++c) {
      for (let r = 0; r < GameWorld.BRICK_ROW_COUNT; ++r) {
        this.objects.push(
          new Brick(
            c * (GameWorld.BRICK_WIDTH + GameWorld.BRICK_PADDING) + GameWorld.BRICK_OFFSET_LEFT,
            r * (GameWorld.BRICK_HEIGHT + GameWorld.BRICK_PADDING) + GameWorld.BRICK_OFFSET_TOP,
            GameWorld.BRICK_WIDTH,
            GameWorld.BRICK_HEIGHT
          )
        );
      }
    }
  }

  /**
   * @private
   */
  update() {
    this.objects.forEach(obj => {
      obj.update();
    });
  }

  /**
   * @private
   */
  drawScore() {
    this.ctx.font = '16px Arial';
    this.ctx.fillStyle = '#0095DD';
    this.ctx.fillText('Score: ' + this.score, 8, 20);
  }

  /**
   * @private
   */
  drawLives() {
    this.ctx.font = '16px Arial';
    this.ctx.fillStyle = '#0095DD';
    this.ctx.fillText('Lives: ' + this.lives, this.width - 65, 20);
  }

  /**
   * @private
   */
  render(n) {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.drawScore();
    this.drawLives();
    this.objects.forEach(obj => {
      obj.draw(this.ctx, n);
    });
  }

  /**
   * @private
   */
  loop() {
    let lastTime = Date.now();
    let lag = 0;
    const loopInner = () => {
      const currentTime = Date.now();
      const elapsed = currentTime - lastTime;
      lag += elapsed;

      if (this.score === GameWorld.BRICK_ROW_COUNT * GameWorld.BRICK_COLUMN_COUNT) {
        alert('You win');
        this.stop();
        return;
      }
      if (this.lives === 0) {
        alert('Game over');
        this.stop();
        return;
      }

      while (lag >= GameWorld.MS_PER_UPDATE) {
        this.update();
        lag -= GameWorld.MS_PER_UPDATE;
        this.leftPressed = false;
        this.rightPressed = false;
        this.paddleRelativeX = undefined;
      }

      lastTime = currentTime;
      this.render(lag / GameWorld.MS_PER_UPDATE);

      this.animationRequestId = requestAnimationFrame(loopInner);
    };

    this.animationRequestId = requestAnimationFrame(loopInner);
  }

  /**
   * @public
   */
  get width() {
    return this.canvas.width;
  }

  /**
   * @public
   */
  get height() {
    return this.canvas.height;
  }

  /**
   * @public
   */
  start() {
    if (this.gameState !== 'stopped') {
      return;
    }
    this.gameState = 'started';
    this.loop();
  }

  /**
   * @public
   */
  stop() {
    if (this.gameState !== 'started') {
      return;
    }
    this.gameState = 'stopped';
    this.generate();
    cancelAnimationFrame(this.animationRequestId);
  }

  /**
   * @public
   */
  restart() {
    this.stop();
    this.start();
  }

  /**
   * @public
   */
  pause() {
    if (this.gameState !== 'started') {
      return;
    }
    this.gameState = 'paused';
    cancelAnimationFrame(this.animationRequestId);
  }

  /**
   * @public
   */
  resume() {
    if (this.gameState !== 'paused') {
      return;
    }
    this.gameState = 'started';
    this.loop();
  }
}
