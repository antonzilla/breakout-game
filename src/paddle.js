import overlaps, {Rect, Line} from './physics';

export default class Paddle {

  constructor(world) {
    this.world = world;
    this.width = 75;
    this.height = 10;
    this.x = (world.width - this.width) / 2;
    this.y = world.height - this.height;
    this.dx = 0;
  }

  update() {
    const leftSide = new Line(0, 0, 0, this.world.height);
    const rightSide = new Line(this.world.width, 0, this.world.width, this.world.height);

    if (this.world.paddleRelativeX != null) {
      this.dx = this.world.paddleRelativeX - this.width / 2 - this.x;

    } else if (this.world.rightPressed) {
      this.dx = 10;

    } else if (this.world.leftPressed) {
      this.dx = -10;
    }

    this.x += this.dx;
    this.dx = 0;

    const paddleShape = this.getShape();
    if (overlaps(paddleShape, leftSide)) {
      this.x = 0;

    } else if (overlaps(paddleShape, rightSide)) {
      this.x = this.world.width - this.width;
    }
  }

  draw(ctx, n) {
    ctx.beginPath();
    ctx.rect(this.x + n * this.dx, this.y, this.width, this.height);
    ctx.fillStyle = '#0095DD';
    ctx.fill();
    ctx.closePath();
  }

  getShape() {
    return new Rect(this.x, this.y, this.width, this.height);
  }
}
