import overlaps, {Line, Circle} from './physics';
import Paddle from './paddle';
import Brick from './brick';

export default class Ball {

  constructor(world) {this.world = world;
    this.x = world.width / 2;
    this.y = world.height - 30;
    this.r = 10;
    this.dx = 2;
    this.dy = -2;
  }

  update() {
    const ballShape = this.getShape();
    const topSide = new Line(0, 0, this.world.width, 0);
    const bottomSide = new Line(0, this.world.height, this.world.width, this.world.height);
    const leftSide = new Line(0, 0, 0, this.world.height);
    const rightSide = new Line(this.world.width, 0, this.world.width, this.world.height);

    if (overlaps(ballShape, leftSide) || overlaps(ballShape, rightSide)) {
      this.dx = -this.dx;

    } else if (overlaps(ballShape, topSide)) {
      this.dy = -this.dy;

    } else if (overlaps(ballShape, bottomSide)) {
      this.dy = -this.dy;
      this.world.lives--;

    } else {
      this.world.objects.forEach(obj => {
        if (obj instanceof Paddle) {
          if (this.dy > 0 && overlaps(ballShape, obj.getShape())) {
            this.dy = -this.dy;
          }

        } else if (obj instanceof Brick) {
          if (!obj.destroyed && overlaps(ballShape, obj.getShape())) {
            this.world.score++;
            this.dy = -this.dy;
            obj.destroy();
          }
        }
      });
    }

    this.x += this.dx;
    this.y += this.dy;
  }

  draw(ctx, n) {
    ctx.beginPath();
    ctx.arc(this.x + this.dx * n, this.y + this.dy * n, this.r, 0, Math.PI * 2);
    ctx.fillStyle = '#0095DD';
    ctx.fill();
    ctx.closePath();
  }

  getShape() {
    return new Circle(this.x, this.y, this.r);
  }
}
